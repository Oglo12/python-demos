"""
How it works:

Prints out some text, and with a combination of "\033[A" (up one line), and "\r" (return to beginning of line) it prints out some text that stays, and some text that gets overwritten.
"""

from time import sleep
from random import randint, choice

modes = ["/", "-", "\\", "|"]
mode = 0

fake_info = [
        "Some files verified.",
        "Double checking.",
        "Data stolen.",
        "Badass hacker impression given to passer-by.",
        "Sang a tune about how you should not hit Ctrl+C.",
        "Handing over data to Google.",
        "Secretly installing Windows 11.",
        "Frying your SSD so you buy a new one.",
        "Making you subscribe to the US Government's YouTube channel.",
        "Located your dad with the milk.",
        "Hacked the house next door over.",
        "Replaced SystemD with OpenRC.",
        "Pi, Pi, it's a Linux Pi. Beat my payloads to the CPU when it starts to fry.",
        ]

fake_loading = [
        "Loading assets...",
        "Loading account...",
        "Loading pets...",
        "Saving cloud imperfections...",
        "Patching game...",
        "Scanning OS...",
        "Installing GCC...",
        "Installing Python and Pip...",
        "Installing Haskell...",
        "Humming a tune...",
        "Drinking on the job...",
        "Stealing user data...",
        "Phoning home to Microsoft...",
        ]

current_loading = choice(fake_loading)

iteration = 0

ran_info_once = False

percent = 0

while True:
    if randint(0, 100) > 95:
        print(" " * 100, end = "\r")

        if ran_info_once == True:
            print("\033[A", end = "")
        else:
            ran_info_once = True

        print(f"[ INFO ]: {choice(fake_info)}\n")
    else:
        print(" " * 100, end = "\r")
        print(f"{current_loading} [{modes[mode]}] {percent}%", end = "\r")

        if randint(0, 100) > 20:
            percent += randint(1, 10)

    if percent >= 100:
        current_loading = choice(fake_loading)
        percent = 0

    if mode == len(modes) - 1:
        mode = 0
    else:
        mode += 1

    iteration += 1

    sleep(randint(2, 20) * 0.01)
